﻿using UnityEngine;
using System.Collections;

public class DisableOnCommand : MonoBehaviour {


    //Disable ui for title, keyword, description;
    public EventActivator disableHelper;

    //Disable ui for change slide, pause ..;
    public EventActivator disableUIButton;

    //Disable all ui
    public EventActivator disableAllUI;


    void Start() {

     //   DisableAllUI();
    }


    public void DisableAllUI()
    {
        if (disableAllUI) disableAllUI.Activate();
    }
    public void DisableButtonUI()
    {
        if (disableUIButton) disableUIButton.Activate();
    }
    public void DisableHelperUI()
    {
        if (disableHelper) disableHelper.Activate();
    }
}
