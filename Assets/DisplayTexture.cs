﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayTexture : MonoBehaviour {

    public Texture textureToDisplay;
    public ScaleMode scaleToApply;
    public Sprite sprite;
    public Image image;

    void Start() { 
    
    }

    void OnGUI(){

        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), textureToDisplay, scaleToApply);
    
    }
}
