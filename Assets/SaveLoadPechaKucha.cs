﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SaveLoadPechaKucha : MonoBehaviour
{

    public PechaKuchaDataMono m_defaul;
    public PechaKuchaDataMono m_user;
    
    public string GetStorageFile()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        return Directory.GetCurrentDirectory()+"PechaKucha";
#else

        return Application.dataPath + "/../PechaKucha";
#endif

    }
    public Image m_image;
    public Text m_text;
    public void Save()
    {

        string dirPath = GetStorageFile();
        string json = JsonUtility.ToJson(m_user.data);
        PlayerPrefs.SetString("PechaKucha", json);

        m_text.text = dirPath +" .. "+ Application.dataPath+ ".. "+ Application.persistentDataPath;
        m_image.color = Color.red;
        File.WriteAllText(Application.dataPath + "/A.txt", "Hey mon ami");
        m_image.color = Color.green;
        File.WriteAllText(Application.persistentDataPath + "/Time.txt", "" + System.DateTime.Now.ToShortDateString());
        File.WriteAllText(Application.dataPath + "/PechaKucha.json", json);

        m_image.color = Color.gray;

        Directory.CreateDirectory(dirPath);

        m_image.color = Color.blue;
        File.Create(dirPath + "D.txt");
        File.WriteAllText(dirPath + "/A.txt", "Hey mon ami");
        File.WriteAllText(dirPath + "/Time.txt", "" + System.DateTime.Now.ToShortDateString());
        File.WriteAllText(dirPath + "/PechaKucha.json", json);
       

        m_image.color = Color.cyan; 
        //for (int i = 0; i < 20; i++)
        //{
        //    Texture2D text = m_user.data.GetTexture(i);
        //    if (text == null)
        //        text = m_defaul.data.GetTexture(i);

        //    Texture2D newText = new Texture2D(text.width, text.height);
        //    newText.SetPixels(text.GetPixels());
        //    newText.Apply();

        //    File.WriteAllBytes(dirPath+"/"+i + ".png", newText.EncodeToPNG());

        //}


    }

    public void Load()
    {
        string dirPath = GetStorageFile();
        Directory.CreateDirectory(dirPath);
        string jsonFilePath = dirPath + "/PechaKucha.json";

        
        if (!File.Exists(jsonFilePath))
            Save();
        Debug.Log("## " + jsonFilePath);
        string json = File.ReadAllText(jsonFilePath);

        if(string.IsNullOrEmpty(json)) json= PlayerPrefs.GetString("PechaKucha");


        m_user.data = JsonUtility.FromJson<PechaKuchaData>(json);
            
            
        for (int i = 0; i < 20; i++)
        {
            string filePath = dirPath + "/" + i + ".png";
            if (File.Exists(filePath)) {
                byte[] textByte = File.ReadAllBytes(filePath);
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(textByte);
                 m_user.data.m_slide[i].m_texture = tex;
            }


            
        }

    }


    public void Awake()
    {
       // Load();
    }

 


}
