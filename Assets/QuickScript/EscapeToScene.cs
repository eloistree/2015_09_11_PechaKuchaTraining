﻿using UnityEngine;
using System.Collections;

public class EscapeToScene : MonoBehaviour {


    public LoadScene loadScene;
    public bool withDelay;

    void Update () {

        if (loadScene!=null &&  Input.GetKeyDown(KeyCode.Escape)) {
            if (withDelay)
                loadScene.LoadSelectedSceneWithDelay();
            else 
                loadScene.LoadSelectedScene();
            return;
        }
	}
}
