﻿using UnityEngine;
using System.Collections.Generic;
[System.Serializable]
public class PechaKucha  {

    public static Dictionary<string, PechaKucha> Registered = new Dictionary<string, PechaKucha>();
    public static PechaKucha Selected;

    public string _title= "Unnamed";
    public Texture2D _backGroundImage;
    public Texture2D[] _20_Image;

//    public string[] _20_slideNameHelper;
//    public string[] _20_DescriptionHelper;
//    public List<string> _20_KeyworkHelper;

	// Use this for initialization
    public PechaKucha(string title, Texture2D[] images=null, Texture2D background =null)
    {

        SetValues(title, images, background);

        if (!Registered.ContainsKey(title))
            Registered.Add(title, this);
        else Registered[title] = this;

        if (Selected == null)
            Selected = this;
	   
	}

    public void SetValues(string title, Texture2D[] images=null, Texture2D background = null)
    {
        _title = title;
        _20_Image = new Texture2D[20];
        if(images==null)
            images = new Texture2D[20];
        int i = 0;
        while (i<20 && i<images.Length)
        {
            _20_Image[i] = images[i];
            i++;
        }
        

        _backGroundImage = background;
       
    }







    internal static PechaKucha[] GetAll()
    {
        PechaKucha [] result = new PechaKucha[Registered.Values.Count];
        Registered.Values.CopyTo(result, 0);
        return result;
    }

    internal Texture2D GetFirstImage()
    {
        Texture2D firstImageValide =null;
        int i=0;
        while (i < _20_Image.Length && !firstImageValide) {
            if (_20_Image[i] != null)
                firstImageValide = _20_Image[i];
            i++;
        }
        return firstImageValide;
    }
}
