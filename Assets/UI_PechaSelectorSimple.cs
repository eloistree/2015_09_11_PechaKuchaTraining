﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_PechaSelectorSimple : MonoBehaviour {


 

    public Text tPechaTitle;
    public Image iFirstSlide;

    public Image buttonPrevious;
    public Image buttonNext;

    public void SetUITo(string title, Texture2D image, bool hasPrevious, bool hasNext) {

        if (tPechaTitle) {
            tPechaTitle.gameObject.SetActive(true);
            tPechaTitle.text = title;
        }
                 if (iFirstSlide){
                     iFirstSlide.gameObject.SetActive(true);
         //   iFirstSlide.sprite = image;
                 }
        if(buttonPrevious){
            Color setToColor = hasPrevious ? Color.white : Color.gray;
            setToColor.a= hasPrevious?1f:0.2f;
            buttonPrevious.gameObject.SetActive(true);
            buttonPrevious.color = setToColor;
            
            Button buttonLinked = buttonPrevious.GetComponent<Button>();
            if(buttonLinked)buttonLinked.interactable = hasPrevious;
        }
        if (buttonNext)
        {
            
            Color setToColor = hasNext ? Color.white : Color.gray;
            setToColor.a= hasNext?1f:0.2f;
            buttonNext.gameObject.SetActive(true);
            buttonNext.color = setToColor;
            Button buttonLinked = buttonNext.GetComponent<Button>();
            if (buttonLinked) buttonLinked.interactable = hasNext;
        }
    }

    internal void SetAsNoneLoaded()
    {

        tPechaTitle.text = "No Project Found";
        iFirstSlide.gameObject.SetActive(false);
        buttonNext.gameObject.SetActive(false);
        buttonPrevious.gameObject.SetActive(false);
    }
}
